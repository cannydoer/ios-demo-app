//
//  OfferPopUpViewController.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/15/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+ENPopUp.h"
#import "OfferPopUpViewController.h"
#import "CGPointVector.h"
#import "OSMCampaign.h"
#import "OSMPayload.h"

#define ANGLE_THRESHOLD 50
#define DISTANCE_THRESHOLD 50

#define INDEX_OR_DEFAULTS 1

@interface OfferPopUpViewController () {
    CGPoint originalCenter;
    CGPoint firstTouchPoint;
    CGPoint currentTouchPoint;
    
    UIViewController *superView;
    
    CGPoint upVector;
    CGPoint rightVector;
    CGPoint leftVector;
    
    UIView *glowView;
    
    OSMCampaign *currentCampaign;
    
    NSMutableArray *offerUUIDs;
    
    int index;
}

@end

@implementation OfferPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // view of the root
    superView = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    
    // vectors to test where the view is pointing
    upVector = CGPointMake(0, 1);
    rightVector = CGPointMake(-1, 0);
    leftVector = CGPointMake(1, 0);
    
    // set up glow view to indicate that a user is in a swipe zone (up, right, left)
    glowView = [[UIView alloc] initWithFrame:superView.view.frame];
    glowView.alpha = 0;
    [superView.view addSubview:glowView];
    
    index = 0;
    
    // load campaign data
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showAllOffers"]) {
        // user default for selectedPin will be UUID of campaign, not offer, so use this as reference in this case
        // a bit hacky imo
        for (OSMCampaign *campaign in [OSMPayload defaultPayload].campaigns) {
            if ([[campaign.uuid UUIDString] isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"selectedPin"]]) {
                currentCampaign = campaign;
                NSLog(@"found it!");
                // put all offer UUIDs into an array to access later
                offerUUIDs = [[NSMutableArray alloc] init];
                for (OSMOffer *offer in currentCampaign.offers) [offerUUIDs addObject:offer];
            }
        }
        // show the information for the first offer
        // otherwise show offer
        OSMOffer *offer = [offerUUIDs objectAtIndex:index];
        self.logoImage.image = currentCampaign.account.media;
        self.mainImage.image = offer.media;
        self.titleLabel.text = offer.title;
        self.subtitleLabel.text = offer.text;
        
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"blur" object:nil];
        [self loadOfferFromDefaults];
    }
    
    // set system state to showing modal
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"modalActive"];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    originalCenter = self.view.center;
    firstTouchPoint = [[touches anyObject] locationInView:superView.view];
    currentTouchPoint = [[touches anyObject] locationInView:superView.view];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // get the distance of the new touch and apply it to the center of the view to move it
    CGPoint touchPoint = [[touches anyObject] locationInView:superView.view];
    CGPoint difference = CGPointSubtract(touchPoint, currentTouchPoint);
    self.view.center = CGPointAdd(self.view.center, difference);
    
    // prevent frame from going to high up the top of the screen due to autolayout issues
    if (self.view.frame.origin.y <= 20) self.view.frame = CGRectMake(self.view.frame.origin.x, 20, self.view.frame.size.width, self.view.frame.size.height);
    
    // display if the user is in a swipe area by presenting a different color for up, left, and right
    if ([self view:self.view IsPointingTowards:upVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:205.0f/255.0f green:236.0f/255.0f blue:241.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else if ([self view:self.view IsPointingTowards:leftVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:197.0f/255.0f green:179.0f/255.0f blue:177.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else if ([self view:self.view IsPointingTowards:rightVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:178.0f/255.0f green:198.0f/255.0f blue:186.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.alpha = 0;
        }];
    }
    
    // update touch point
    currentTouchPoint = touchPoint;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if ([self view:self.view IsPointingTowards:upVector FromPoint:originalCenter]) {
        [self currentOffer].like = @"maybe";
        NSLog(@"maybe liked this");
        [self animateAwayView:self.view];
    }
    else if ([self view:self.view IsPointingTowards:leftVector FromPoint:originalCenter]) {
        [self currentOffer].like = @"no";
        NSLog(@"didn't like it");
        [self animateAwayView:self.view];
    }
    else if ([self view:self.view IsPointingTowards:rightVector FromPoint:originalCenter]) {
        [self currentOffer].like = @"yes";
        NSLog(@"liked it");
        [self animateAwayView:self.view];
    }
    else {
        // reset view to center of screen (original center)
        self.view.userInteractionEnabled = NO;
        
        // put new info there if need be
        
        [UIView animateWithDuration:0.1 animations:^{
            self.view.center = originalCenter;
            glowView.alpha = 0;
        } completion:^(BOOL finished) {
            self.view.userInteractionEnabled = YES;
        }];
    }
}

- (void)animateAwayView:(UIView *)view {
    [view setUserInteractionEnabled:NO];
    index++;
    
    // find direction to push view away
    CGPoint difference = CGPointSubtract(view.center, originalCenter);
    CGPoint multiple = CGPointMultiply(difference, 3);
    CGPointNormalize(multiple);
    
    // push and fade view away
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        glowView.alpha = 0;
        view.center = CGPointAdd(view.center, multiple);
        view.alpha = 0;
    } completion:^(BOOL finished) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showAllOffers"] && (index < offerUUIDs.count)) {
            view.center = originalCenter;
            
            OSMOffer *offer = [offerUUIDs objectAtIndex:index];
            self.logoImage.image = currentCampaign.account.media;
            self.mainImage.image = offer.media;
            self.titleLabel.text = offer.title;
            self.subtitleLabel.text = offer.text;
            
            [UIView animateWithDuration:0.1 animations:^{
                view.alpha = 1;
            } completion:^(BOOL finished) {
                [view setUserInteractionEnabled:YES];
            }];
        }
        else {
            // dismiss because it's using defaults
            [glowView removeFromSuperview];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"showAllOffers"];
            [self dismissPopUpViewController];
        }
        
    }];
}

// Determines if offer view is in the proper area to be 'liked', 'disliked', or 'maybe liked'
-(BOOL)view:(UIView *)view IsPointingTowards:(CGPoint)vector FromPoint:(CGPoint)startingPoint {
    // Calculate the current angle
    CGPoint distanceFromStartingPoint = CGPointSubtract(startingPoint, view.center);
    CGFloat angle = CGPointGetAngleBetween(distanceFromStartingPoint, vector) * (180 / M_PI);
    
    // Color view appropriately if it is within the 'good' zone
    if (angle < ANGLE_THRESHOLD && CGPointGetDistance(startingPoint, view.center) > DISTANCE_THRESHOLD) return true;
    else return false;
}

- (void)loadOfferFromDefaults {
    // find selected offer in payload from userdefaults
    OSMPayload *payload = [OSMPayload defaultPayload];
    OSMOffer *selectedOffer;
    OSMCampaign *selectedCampaign;
    for (OSMCampaign *campaign in payload.campaigns) {
        for (OSMOffer *offer in campaign.offers) {
            if ([[offer.uuid UUIDString] isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"selectedPin"]]) {
                // offer and campaign found. store and break from loop
                selectedOffer = offer;
                selectedCampaign = campaign;
                break;
            }
        }
    }
    
    if (selectedOffer == nil || selectedCampaign == nil) {
        // dismiss because the offer is invalid
        [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"showAllOffers"];
        [self dismissPopUpViewController];
    }
    
    // otherwise show offer
    self.logoImage.image = selectedCampaign.account.media;
    self.mainImage.image = selectedOffer.media;
    self.titleLabel.text = selectedOffer.title;
    self.subtitleLabel.text = selectedOffer.text;
    
}

- (OSMOffer *)currentOffer {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showAllOffers"]) return [offerUUIDs objectAtIndex:index];

    OSMPayload *payload = [OSMPayload defaultPayload];
    OSMOffer *selectedOffer;
    for (OSMCampaign *campaign in payload.campaigns) {
        for (OSMOffer *offer in campaign.offers) {
            if ([[offer.uuid UUIDString] isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"selectedPin"]]) {
                // offer and campaign found. store and break from loop
                selectedOffer = offer;
                break;
            }
        }
    }
    return selectedOffer;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)facebookButtonPressed:(id)sender {
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
    //[self dismissPopUpViewController];
}

- (IBAction)twitterButtonPressed:(id)sender {
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
    //[self dismissPopUpViewController];
}
@end
