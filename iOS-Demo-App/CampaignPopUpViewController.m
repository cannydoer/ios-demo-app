//
//  CampaignPopUpViewController.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/15/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+ENPopUp.h"
#import "CampaignPopUpViewController.h"
#import "CGPointVector.h"
#import "OSMCampaign.h"
#import "OSMPayload.h"

#define ANGLE_THRESHOLD 50
#define DISTANCE_THRESHOLD 50

#define INDEX_OR_DEFAULTS 1

@interface CampaignPopUpViewController () {
    CGPoint originalCenter;
    CGPoint firstTouchPoint;
    CGPoint currentTouchPoint;
    
    UIViewController *superView;
    
    CGPoint upVector;
    CGPoint rightVector;
    CGPoint leftVector;
    
    UIView *glowView;
    
    OSMCampaign *currentCampaign;
}

@end

@implementation CampaignPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"blur" object:nil];
    
    // view of the root
    superView = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
    
    // vectors to test where the view is pointing
    upVector = CGPointMake(0, 1);
    rightVector = CGPointMake(-1, 0);
    leftVector = CGPointMake(1, 0);
    
    // set up glow view to indicate that a user is in a swipe zone (up, right, left)
    glowView = [[UIView alloc] initWithFrame:superView.view.frame];
    glowView.alpha = 0;
    [superView.view addSubview:glowView];
    
    currentCampaign = [self currentCampaign];
    
    // load campaign data
    [self loadOfferFromDefaults];
    
    // set system state to showing modal
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"modalActive"];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    originalCenter = self.view.center;
    firstTouchPoint = [[touches anyObject] locationInView:superView.view];
    currentTouchPoint = [[touches anyObject] locationInView:superView.view];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // get the distance of the new touch and apply it to the center of the view to move it
    CGPoint touchPoint = [[touches anyObject] locationInView:superView.view];
    CGPoint difference = CGPointSubtract(touchPoint, currentTouchPoint);
    self.view.center = CGPointAdd(self.view.center, difference);
    
    // prevent frame from going to high up the top of the screen due to autolayout issues
    if (self.view.frame.origin.y <= 20) self.view.frame = CGRectMake(self.view.frame.origin.x, 20, self.view.frame.size.width, self.view.frame.size.height);
    
    // display if the user is in a swipe area by presenting a different color for up, left, and right
    if ([self view:self.view IsPointingTowards:upVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:205.0f/255.0f green:236.0f/255.0f blue:241.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else if ([self view:self.view IsPointingTowards:leftVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:197.0f/255.0f green:179.0f/255.0f blue:177.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else if ([self view:self.view IsPointingTowards:rightVector FromPoint:originalCenter]) {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.backgroundColor = [UIColor colorWithRed:178.0f/255.0f green:198.0f/255.0f blue:186.0f/255.0f alpha:1];
            glowView.alpha = 0.5;
        }];
    }
    else {
        [UIView animateWithDuration:0.2 animations:^{
            glowView.alpha = 0;
        }];
    }
    
    // update touch point
    currentTouchPoint = touchPoint;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if ([self view:self.view IsPointingTowards:upVector FromPoint:originalCenter]) {
        currentCampaign.like = @"maybe";
        [self animateAwayView:self.view];
    }
    else if ([self view:self.view IsPointingTowards:leftVector FromPoint:originalCenter]) {
        currentCampaign.like = @"no";
        [self animateAwayView:self.view];
    }
    else if ([self view:self.view IsPointingTowards:rightVector FromPoint:originalCenter]) {
        currentCampaign.like = @"yes";
        [self animateAwayView:self.view];
    }
    else {
        // reset view to center of screen (original center)
        self.view.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.1 animations:^{
            self.view.center = originalCenter;
            glowView.alpha = 0;
        } completion:^(BOOL finished) {
            self.view.userInteractionEnabled = YES;
        }];
    }
}

- (void)animateAwayView:(UIView *)view {
    [view setUserInteractionEnabled:NO];
    
    // find direction to push view away
    CGPoint difference = CGPointSubtract(view.center, originalCenter);
    CGPoint multiple = CGPointMultiply(difference, 3);
    CGPointNormalize(multiple);
    
    // push and fade view away
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        view.center = CGPointAdd(view.center, multiple);
        view.alpha = 0;
    } completion:^(BOOL finished) {
        
        // dismiss because it's using defaults
        [glowView removeFromSuperview];
        
        if ([currentCampaign.like isEqualToString:@"yes"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showOffers" object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
        }
        [self dismissPopUpViewController];
        
    }];
}

// Determines if offer view is in the proper area to be 'liked', 'disliked', or 'maybe liked'
-(BOOL)view:(UIView *)view IsPointingTowards:(CGPoint)vector FromPoint:(CGPoint)startingPoint {
    // Calculate the current angle
    CGPoint distanceFromStartingPoint = CGPointSubtract(startingPoint, view.center);
    CGFloat angle = CGPointGetAngleBetween(distanceFromStartingPoint, vector) * (180 / M_PI);
    
    // Color view appropriately if it is within the 'good' zone
    if (angle < ANGLE_THRESHOLD && CGPointGetDistance(startingPoint, view.center) > DISTANCE_THRESHOLD) return true;
    else return false;
}

- (void)loadOfferFromDefaults {

    if (currentCampaign == nil) {
        // dismiss because the offer is invalid
        [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
        [self dismissPopUpViewController];
    }
    
    // otherwise show offer
    self.logoImage.image = currentCampaign.account.media;
    self.mainImage.image = currentCampaign.media;
    self.titleLabel.text = currentCampaign.name;
    //self.subtitleLabel.text = selectedCampaign.text;
    
}

- (OSMCampaign *)currentCampaign {
    OSMPayload *payload = [OSMPayload defaultPayload];
    OSMCampaign *selectedCampaign;
    for (OSMCampaign *campaign in payload.campaigns) {
        if ([[campaign.uuid UUIDString] isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"selectedPin"]]) {
            selectedCampaign = campaign;
            break;
        }
    }
    return selectedCampaign;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)likeButtonPressed:(id)sender {
    currentCampaign.like = @"yes";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showOffers" object:nil];
    [self dismissPopUpViewController];
}

- (IBAction)dislikeButtonPressed:(id)sender {
    currentCampaign.like = @"no";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unblur" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showOffers" object:nil];
    [self dismissPopUpViewController];
}
@end
