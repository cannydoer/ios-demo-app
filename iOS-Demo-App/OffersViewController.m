//
//  OffersViewController.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/14/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "OffersViewController.h"
#import "OffersTableViewCell.h"
#import "OSMPayload.h"

@import MapKit;

@interface OffersViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation OffersViewController {
    OSMPayload *payload;
    NSMutableArray *offerArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"unblur" object:nil];
    
    payload = [OSMPayload defaultPayload];
    
    // compile all offers into a single array
    offerArray = [[NSMutableArray alloc] init];
    [self loadData: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData: (NSNotification *)notification {
    [offerArray removeAllObjects];
    for (OSMCampaign *campaign in payload.campaigns) {
        for (OSMOffer *offer in campaign.offers) {
            if ([offer.like isEqualToString:@"like"] || [offer.like isEqualToString:@"yes"]) [offerArray addObject:offer];
        }
    }
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return offerArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // load wishlist cells
    OffersTableViewCell *cell = (OffersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OffersTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // populate cell
    OSMOffer *offer = [offerArray objectAtIndex:indexPath.row];
    OSMCampaign *campaign = [payload.campaigns objectAtIndex:0];
    cell.logoImageView.image = campaign.account.media;
    cell.advertiserLabel.text = campaign.account.name;
    cell.titleLabel.text = offer.title;
    cell.subtitleLabel.text = offer.text;
    [cell.locationButton setTitle:offer.address forState:UIControlStateNormal];
    
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // place offer back on map by deleting the contents in .like
        OSMOffer *offer = [offerArray objectAtIndex:indexPath.row];
        offer.like = @"";
        [offerArray removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMap" object:nil];
    }
}


// present map when tapped
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OSMOffer *offer = [offerArray objectAtIndex:indexPath.row];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:offer.address completionHandler:^(NSArray* placemarks, NSError* error){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
        MKPlacemark *mapPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:mapPlacemark];
        mapItem.name = offer.title;
        [mapItem openInMapsWithLaunchOptions:nil];
    }];
    
   
}

@end
