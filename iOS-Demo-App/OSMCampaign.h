//
//  OSMCampaign.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSMAccount.h"
#import "OSMOffer.h"

@interface OSMCampaign : NSObject

@property (strong, nonatomic) NSUUID *uuid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *like;
@property (strong, nonatomic) OSMAccount *account;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) UIImage *media;
@property (strong, nonatomic) NSDate *effectiveDate;
@property (strong, nonatomic) NSDate *expirationDate;
@property (strong, nonatomic) NSArray *offers;
@property (strong, nonatomic) NSString *createdBy;

@property (strong, nonatomic) NSArray *markers;

- (id)initWithDictionary:(NSDictionary *)dictionary;

+ (id)defaultPayload;

@end
