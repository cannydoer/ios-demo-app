//
//  OffersTableViewCell.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/15/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIStackView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *advertiserLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

- (IBAction)locationButtonPressed:(id)sender;

@end
