//
//  MapViewController.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/14/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "UIViewController+ENPopUp.h"
#import "CampaignPopUpViewController.h"
#import "MapViewController.h"
#import "OSMPayload.h"
#import <QuartzCore/QuartzCore.h>
#import <Toast/UIView+Toast.h>

#define UPDATE_DISTANCE_METERS 1000
#define OFFER_NOTIFICATION_RADIUS_METERS 100
#define IN_APP_DURATION 3

@interface MapViewController () <CLLocationManagerDelegate, MKMapViewDelegate> {
    CLLocationManager *locationManager;
    CLLocation *lastLocation;
    
    UIVisualEffectView *visualEffectView;
    
    OSMPayload *payload;
    
    bool toastIsVisible;
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set up map
    self.mapView.delegate = self;
    self.mapView.showsBuildings = YES;
    self.mapView.showsUserLocation = YES;
    self.mapView.rotateEnabled = NO;
    
    // Set up location services
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.distanceFilter = 1000;
    
    // Figure out if authorization has been made
    
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)) {
        [locationManager requestAlwaysAuthorization];
    }
    else {
        // Start updating location
        [locationManager startUpdatingLocation];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(blur:) name:@"blur" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unblur:) name:@"unblur" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMap:) name:@"reloadMap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showOffers:) name:@"showOffers" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationTapped:) name:@"localNotificationTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inAppNotificationTapped:) name:@"inAppNotificationTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedLocation:) name:@"updatedLocation" object:nil];
    
    // load all campaigns and offers
    payload = [OSMPayload defaultPayload];
    NSLog(@"%lu", (unsigned long)payload.campaigns.count);
    
    // load annotations for purposes of demo
    [self loadAnnotations];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
    }
}

-(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    MKAnnotationView *ulv = [mapView viewForAnnotation:mapView.userLocation];
    ulv.hidden = YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    // Set map to center on user location
    NSLog(@"Updated coordinates to: Lat: %f, Long: %f", locations.lastObject.coordinate.latitude, locations.lastObject.coordinate.longitude);
    
    [self updateMapForLocation:[locations lastObject]];
    
    // Test differences in locations and see if the distance is great enough to trigger a GET request for more ads
    if (lastLocation == nil) lastLocation = locations.lastObject;
    if ([locations.lastObject distanceFromLocation:lastLocation] >= UPDATE_DISTANCE_METERS) {
        [self loadAnnotations];
        lastLocation = locations.lastObject;
    }
    
    // Also test if the user is within the range of any markers, and if so, trigger a notification
    for (MKPointAnnotation *annotation in self.mapView.annotations) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        NSLog(@"distance from annotation: %f", [locations.lastObject distanceFromLocation:location]);
        if ([locations.lastObject distanceFromLocation:location] <= OFFER_NOTIFICATION_RADIUS_METERS) {
            
            NSLog(@"annotation accessibility hint: %@", annotation.accessibilityHint);
            
            // Pull the offer / campaign data. Store UUID for showing content if user taps toast
            
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"localNotification"]) {
            
                [[NSUserDefaults standardUserDefaults] setObject:annotation.accessibilityHint forKey:@"notificationUUID"];
                [[NSUserDefaults standardUserDefaults] setObject:annotation.accessibilityLabel forKey:@"notificationType"];
                
                NSLog(@"content uuid: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"notificationUUID"]);
                NSLog(@"content type: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"]);
            }
            
            // make sure that the notification type is valid, as well as the annotation.hint
            
            if (([[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"campaign"] || [[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"offer"]) && annotation.accessibilityHint) {
            
                // Show notification
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertTitle = @"You're nearby a deal!";
                localNotification.alertBody = @"Tap now to view the exclusive deal in your area.";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
            }

            break;
        }
    }
    
}

- (void)updateMapForLocation:(CLLocation *)location {
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
}

- (void)loadAnnotations {
    //remove current annotations
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    // cycle through campaigns and determine which campaigns to show and which offers to show
    for (OSMCampaign *campaign in payload.campaigns) {
        // if someone likes a campaign, show the offers
        if ([campaign.like isEqualToString:@"yes"] || [campaign.like isEqualToString:@"true"]) {
            // cycle through offers and add them to the mapview
            for (OSMOffer *offer in campaign.offers) {
                if ([offer.like isEqualToString:@"maybe"]) {
                    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                    point.coordinate = CLLocationCoordinate2DMake(offer.marker.latitude, offer.marker.longitude);
                    point.title = offer.title;
                    point.subtitle = offer.text;
                    
                    point.accessibilityHint = [offer.uuid UUIDString];
                    point.accessibilityLabel = @"offer";
                    
                    [self.mapView addAnnotation:point];
                }
            }
        } else {
            // Show campaign
            for (OSMMarker *marker in campaign.markers) {
                MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                point.coordinate = CLLocationCoordinate2DMake(marker.latitude, marker.longitude);
                point.title = campaign.name;
                
                point.accessibilityHint = [campaign.uuid UUIDString];
                point.accessibilityLabel = @"campaign";
                
                [self.mapView addAnnotation:point];
            }
        }
    }
    
    
}

- (MKAnnotationView *)mapView:(MKMapView *)sender viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) return nil;
    
    MKPinAnnotationView *pinView;
    MKPointAnnotation *point;
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        point = (MKPointAnnotation *)annotation;
    }
    
    pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    pinView.canShowCallout = YES;

    if ([point.accessibilityLabel isEqualToString:@"campaign"]) [pinView setPinTintColor:[UIColor blueColor]];
    else if ([point.accessibilityLabel isEqualToString:@"offer"]) [pinView setPinTintColor:[UIColor yellowColor]];
    
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    MKPointAnnotation *tappedPin = view.annotation;
    for (MKPointAnnotation* pin in self.mapView.annotations) {
        NSLog(@"%@", pin.accessibilityLabel);
    }
    // find the object that corresponds to the tapped pin and display that modal
    for (OSMCampaign *campaign in payload.campaigns) {
        if ([[campaign.uuid UUIDString] isEqualToString:tappedPin.accessibilityHint]) {
            NSLog(@"Launch campaign modal");
            [[NSUserDefaults standardUserDefaults] setObject:tappedPin.accessibilityHint forKey:@"selectedPin"];
            UIViewController *campaignView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CampaignPopUp"];
            campaignView.view.frame = CGRectMake(0, 0, 340.0f, 280.0f);
            [self presentPopUpViewController:campaignView];
        }
        else {
            for (OSMOffer *offer in campaign.offers) {
                if ([[offer.uuid UUIDString] isEqualToString:tappedPin.accessibilityHint]) {
                    NSLog(@"Launch offer modal");
                    [[NSUserDefaults standardUserDefaults] setObject:tappedPin.accessibilityHint forKey:@"selectedPin"];
                    UIViewController *offerView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OfferPopUp"];
                    offerView.view.frame = CGRectMake(0, 0, 340.0f, 280.0f);
                    [self presentPopUpViewController:offerView];
                }
            }
        }
    }
    
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    
    NSLog(@"Tapped %@", tappedPin.accessibilityHint);
    
}

#pragma NSNotification Methods
- (void)blur:(NSNotification *)notification {
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];

    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.view.bounds;
    [self.view addSubview:visualEffectView];
    
    visualEffectView.alpha = 0;
    [UIView animateWithDuration:0.2 animations:^{
        visualEffectView.alpha = 1;
    }];
}

- (void)unblur:(NSNotification *)notification {
    visualEffectView.alpha = 1;
    
    [UIView animateWithDuration:0.2 animations:^{
        visualEffectView.alpha = 0;
    } completion:^(BOOL finished) {
        [visualEffectView removeFromSuperview];
    }];
    
    // reload annotations
    payload = [OSMPayload defaultPayload];
    [self loadAnnotations];
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
}

- (void)reloadMap:(NSNotification *)notification {
    [self loadAnnotations];
}

- (void)showOffers:(NSNotification *)notification {
    NSLog(@"Show all of the offers for a campaign.");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showAllOffers"];
    UIViewController *offerView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OfferPopUp"];
    offerView.view.frame = CGRectMake(0, 0, 340.0f, 280.0f);
    [self presentPopUpViewController:offerView];
}

- (void)inAppNotificationTapped:(NSNotification *)notification {
    OSMCampaign *campaign = [payload.campaigns objectAtIndex:0];
    
    // toast with all possible options
    if (!toastIsVisible && ![[NSUserDefaults standardUserDefaults] boolForKey:@"modalActive"]) {
        toastIsVisible = true;
        [self.view makeToast:@"Tap now to view the exclusive deal in your area."
                    duration:IN_APP_DURATION
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, 64)]
                       title:@"You're nearby a deal!"
                       image:campaign.account.media
                       style:nil
                  completion:^(BOOL didTap) {
                      if (didTap) [self localNotificationTapped:nil];
                      toastIsVisible = false;
                  }];
    }
}

- (void)localNotificationTapped:(NSNotification *)notification {
    NSLog(@"Displaying appropriate modal");
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"offer"]) {
        //show offer modal
        NSLog(@"Launch offer modal");
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationUUID"] forKey:@"selectedPin"];
        UIViewController *offerView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OfferPopUp"];
        offerView.view.frame = CGRectMake(0, 0, 340.0f, 280.0f);
        [self presentPopUpViewController:offerView];
    } else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"campaign"]) {
        // show campaign modal
        NSLog(@"Launch campaign modal");
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationUUID"] forKey:@"selectedPin"];
        UIViewController *campaignView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CampaignPopUp"];
        campaignView.view.frame = CGRectMake(0, 0, 340.0f, 280.0f);
        [self presentPopUpViewController:campaignView];
    }
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"localNotification"];
}

- (IBAction)simulateLocationPressed:(id)sender {
    UIViewController *offerView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationPopUp"];
    offerView.view.frame = CGRectMake(0, 0, 340.0f, 500.0f);
    [self presentPopUpViewController:offerView];
}

- (void)updatedLocation:(NSNotification *)notification {
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:[[NSUserDefaults standardUserDefaults] doubleForKey:@"currentLat"] longitude:[[NSUserDefaults standardUserDefaults] doubleForKey:@"currentLng"]];
    
    for (MKPointAnnotation *annotation in self.mapView.annotations) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        NSLog(@"distance from annotation: %f", [currentLocation distanceFromLocation:location]);
        if ([currentLocation distanceFromLocation:location] <= OFFER_NOTIFICATION_RADIUS_METERS) {
            
            NSLog(@"annotation accessibility hint: %@", annotation.accessibilityHint);
            
            // Pull the offer / campaign data. Store UUID for showing content if user taps toast
            
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"localNotification"]) {
                
                [[NSUserDefaults standardUserDefaults] setObject:annotation.accessibilityHint forKey:@"notificationUUID"];
                [[NSUserDefaults standardUserDefaults] setObject:annotation.accessibilityLabel forKey:@"notificationType"];
                
                NSLog(@"content uuid: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"notificationUUID"]);
                NSLog(@"content type: %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"]);
            }
            
            // make sure that the notification type is valid, as well as the annotation.hint
            
            if (([[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"campaign"] || [[[NSUserDefaults standardUserDefaults] stringForKey:@"notificationType"] isEqualToString:@"offer"]) && annotation.accessibilityHint) {
                
                // Show notification
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertTitle = @"You're nearby a deal!";
                localNotification.alertBody = @"Tap now to view the exclusive deal in your area.";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                
            }
            
            break;
        }
    }
}
@end
