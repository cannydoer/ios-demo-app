//
//  OSMCampaign.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "OSMCampaign.h"

@implementation OSMCampaign

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (!dictionary) return nil;
        
        self.uuid = [[NSUUID alloc] initWithUUIDString:dictionary[@"id"]];
        self.name = dictionary[@"name"];
        self.like = (dictionary[@"like"] == [NSNull null]) ? @"" : dictionary[@"like"];
        self.account = [[OSMAccount alloc] initWithDictionary:dictionary[@"account"]];
        self.url = [NSURL URLWithString:dictionary[@"url"]];
        
        self.media = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dictionary[@"media"]]]];
        
        self.effectiveDate = [[NSDate alloc] initWithTimeIntervalSince1970:[dictionary[@"effectiveDate"] integerValue]/1000];
        self.expirationDate = [[NSDate alloc] initWithTimeIntervalSince1970:[dictionary[@"expirationDate"] integerValue]/1000];
        
        self.createdBy = dictionary[@"createdBy"];
        
        NSMutableArray *offers = [[NSMutableArray alloc] init];
        for (NSDictionary *offer in dictionary[@"offers"]) {
            OSMOffer *newOffer = [[OSMOffer alloc] initWithDictionary:offer];
            [offers addObject:newOffer];
        }
        self.offers = (NSArray *)offers;
        
        NSMutableArray *markers = [[NSMutableArray alloc] init];
        for (NSDictionary *d in dictionary[@"markers"]) {
            OSMMarker *marker = [[OSMMarker alloc] init];
            marker.latitude = [d[@"lat"] doubleValue];
            marker.longitude = [d[@"lng"] doubleValue];
            marker.radius = [d[@"radius"] doubleValue];
            [markers addObject:marker];
        }
        self.markers = (NSArray *)markers;
    }
    return self;
}

+ (id)defaultPayload {
    static OSMCampaign *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"json" ofType:@"txt"];
        NSData *jsonData = [NSData dataWithContentsOfFile:path];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        sharedInstance = [[self alloc] initWithDictionary:dict];
    });
    return sharedInstance;
}

@end
