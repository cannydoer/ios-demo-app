//
//  OSMMarker.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 4/5/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OSMMarker : NSObject

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) double radius;

@end
