//
//  OSMAccount.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "OSMAccount.h"

@implementation OSMAccount

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.uuid = [[NSUUID alloc] initWithUUIDString:dictionary[@"id"]];
        
        self.media = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dictionary[@"media"]]]];
        
        self.name = dictionary[@"name"];
    }
    return self;
}

@end
