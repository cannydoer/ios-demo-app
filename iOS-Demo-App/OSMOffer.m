//
//  OSMOffer.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "OSMOffer.h"

@implementation OSMOffer

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.uuid = [[NSUUID alloc] initWithUUIDString:dictionary[@"id"]];
        self.title = dictionary[@"title"];
        self.text = dictionary[@"text"];
        
        self.media = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:dictionary[@"media"]]]];
        
        self.address = dictionary[@"address"];
        self.optedIn = dictionary[@"optedIn"];
        
        self.like = (dictionary[@"like"] == [NSNull null]) ? @"" : dictionary[@"like"];
        
        self.marker = [[OSMMarker alloc] init];
        self.marker.latitude = [dictionary[@"lat"] doubleValue];
        self.marker.longitude = [dictionary[@"lng"] doubleValue];
        self.marker.radius = [dictionary[@"radius"] doubleValue];
    }
    return self;
}

@end
