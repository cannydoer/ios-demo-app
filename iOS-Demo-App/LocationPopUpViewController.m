//
//  LocationPopUpViewController.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 6/12/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "LocationPopUpViewController.h"
#import "UIViewController+ENPopUp.h"

@interface LocationPopUpViewController ()

@end

@implementation LocationPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)USCampaignPressed:(id)sender {
    NSLog(@"yeah");
    [self setLocationToLatitude:39.957436 Longitude:-82.989667];
}

- (IBAction)USOffer1Pressed:(id)sender {
    [self setLocationToLatitude:37.64 Longitude:-81.56];
}

- (IBAction)USOffer2Pressed:(id)sender {
    [self setLocationToLatitude:43.51 Longitude:-111.09];
}

- (IBAction)USOffer3Pressed:(id)sender {
    [self setLocationToLatitude:30.99 Longitude:-81.91];
}

- (IBAction)europeCampaignPressed:(id)sender {
    [self setLocationToLatitude:51.5034070 Longitude:-0.1275920];
}

- (IBAction)europeOffer1Pressed:(id)sender {
    [self setLocationToLatitude:52.20 Longitude:9.84];
}

- (IBAction)europeOffer2Pressed:(id)sender {
    [self setLocationToLatitude:44.52 Longitude:4.22];
}

- (IBAction)europeOffer3Pressed:(id)sender {
    [self setLocationToLatitude:45.26 Longitude:18.63];
}

- (IBAction)dismissPressed:(id)sender {
    [self dismissPopUpViewController];
}

- (void) setLocationToLatitude:(double)lat Longitude:(double)lng {
    [[NSUserDefaults standardUserDefaults] setDouble:lat forKey:@"currentLat"];
    [[NSUserDefaults standardUserDefaults] setDouble:lng forKey:@"currentLng"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedLocation" object:nil];
    [self dismissPopUpViewController];
}
@end
