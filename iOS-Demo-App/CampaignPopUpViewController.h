//
//  CampaignPopUpViewController.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/15/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CampaignPopUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *dislikeButton;

- (IBAction)likeButtonPressed:(id)sender;
- (IBAction)dislikeButtonPressed:(id)sender;

@end
