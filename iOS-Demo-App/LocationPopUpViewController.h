//
//  LocationPopUpViewController.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 6/12/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationPopUpViewController : UIViewController

- (IBAction)USCampaignPressed:(id)sender;
- (IBAction)USOffer1Pressed:(id)sender;
- (IBAction)USOffer2Pressed:(id)sender;
- (IBAction)USOffer3Pressed:(id)sender;

- (IBAction)europeCampaignPressed:(id)sender;
- (IBAction)europeOffer1Pressed:(id)sender;
- (IBAction)europeOffer2Pressed:(id)sender;
- (IBAction)europeOffer3Pressed:(id)sender;

- (IBAction)dismissPressed:(id)sender;

@end
