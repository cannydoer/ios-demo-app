//
//  OSMPayload.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 4/5/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import "OSMPayload.h"

@implementation OSMPayload

+ (instancetype)defaultPayload {
    static OSMPayload *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initStandardPayload];
    });
    return sharedInstance;
}

- (id)initStandardPayload {
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"json" ofType:@"txt"];
        NSData *jsonData = [NSData dataWithContentsOfFile:path];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
        self.campaigns = [[NSMutableArray alloc] init];
        for (NSDictionary *d in dict) {
            [self.campaigns addObject:[[OSMCampaign alloc] initWithDictionary:d]];
        }
    }
    return self;

}

@end
