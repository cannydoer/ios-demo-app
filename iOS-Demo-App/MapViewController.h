//
//  MapViewController.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/14/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;
@import CoreLocation;

@interface MapViewController : UIViewController

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)simulateLocationPressed:(id)sender;

@end
