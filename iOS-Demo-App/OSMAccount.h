//
//  OSMAccount.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface OSMAccount : NSObject

@property (strong, nonatomic) NSUUID *uuid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *media;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
