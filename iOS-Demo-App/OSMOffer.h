//
//  OSMOffer.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/21/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSMMarker.h"
@import CoreLocation;
@import UIKit;

@interface OSMOffer : NSObject

@property (strong, nonatomic) NSUUID *uuid;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *like;
@property (strong, nonatomic) UIImage *media;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *optedIn;
@property (strong, nonatomic) OSMMarker *marker;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
