//
//  main.m
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/14/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
