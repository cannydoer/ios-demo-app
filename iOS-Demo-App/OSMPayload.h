//
//  OSMPayload.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 4/5/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSMCampaign.h"

@interface OSMPayload : NSObject

+ (instancetype)defaultPayload;
- (id)initStandardPayload;

@property (strong, nonatomic) NSMutableArray *campaigns;

@end