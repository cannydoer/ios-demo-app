//
//  OffersViewController.h
//  iOS-Demo-App
//
//  Created by Stuart Farmer on 3/14/16.
//  Copyright © 2016 OnSeen Marketing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
